# coding: utf-8

{
    'name': 'Maintenance Management',
    'version': '8.0.0.1.0',
    "license": "AGPL-3",
    'author': 'Universidad de Carabobo',
    'category': 'Other',
    'description': """
Maintenance Management
======================

Aims to cover the way of reporting failures in equipments
and how to track the evolution of the servicing during its
performance
    """,
    'website': 'http://www.uc.edu.ve/',
    'images': [],
    'depends': [
        'base',
        'product',
        'hr',
        'project',
        'stock',
        'mrp',
    ],
    'data': [
        'security/oomm_security.xml',
        'security/ir.model.access.csv',
        'view/workflow.xml',
        'data/data.xml',
        'view/oomm.xml',
        'view/res_company.xml',
        'view/wizard.xml',
    ],
    'qweb': [
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}
