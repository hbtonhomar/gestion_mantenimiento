# coding: utf-8
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp import SUPERUSER_ID

INCIDENCE_STATES = [
    ('draft', 'Draft'),
    ('cancel', 'Cancel'),
    ('triage', 'Triage'),
    ('submit', 'Submitted to Supervisor'),
    ('inspection', 'Supervisor Inspection'),
    ('assignment', 'Resource Assignment'),
    ('picking_except', 'Picking Exception'),
    ('confirmed', 'Awaiting Raw Materials'),
    ('procurement', 'Resource Procurement'),
    ('purchase', 'Placed Purchase Order'),
    ('ready', 'Crew Delegation'),
    ('waiting', 'Waiting Execution'),
    ('in_production', 'Performing Execution'),
    ('done', 'Done'),
]


class oodoIncidence(osv.osv):

    def _get_default_project(self, cr, uid, context=None):
        context = context or {}
        company_id = self.pool.get('res.users')._get_company(cr, uid,
                                                             context=context)
        if not company_id:
            raise osv.except_osv(
                _('Error!'),
                _('There is no default company for the current user!'))

        rc_obj = self.pool.get('res.company')
        rc_brw = rc_obj.browse(cr, uid, company_id, context=context)

        return rc_brw.project_id and rc_brw.project_id.id

    def _get_default_picking_type(self, cr, uid, context=None):
        context = context or {}
        company_id = self.pool.get('res.users')._get_company(cr, uid,
                                                             context=context)
        if not company_id:
            raise osv.except_osv(
                _('Error!'),
                _('There is no default company for the current user!'))

        rc_obj = self.pool.get('res.company')
        rc_brw = rc_obj.browse(cr, uid, company_id, context=context)

        return rc_brw.picking_type_id and rc_brw.picking_type_id.id

    def _get_default_product(self, cr, uid, context=None):
        context = context or {}
        company_id = self.pool.get('res.users')._get_company(cr, uid,
                                                             context=context)
        if not company_id:
            raise osv.except_osv(
                _('Error!'),
                _('There is no default company for the current user!'))

        rc_obj = self.pool.get('res.company')
        rc_brw = rc_obj.browse(cr, uid, company_id, context=context)

        return rc_brw.product_id and rc_brw.product_id.id

    _inherit = 'mrp.production'
    _description = 'Incidence'
    _columns = {
        'origin': fields.char(
            'Brief Description', readonly=True,
            states={'draft': [('readonly', False)]},
            help="Short Description", copy=False),
        'state': fields.selection(INCIDENCE_STATES, 'Status'),
        'state2': fields.related(
            'state', store=True, type='selection',
            selection=INCIDENCE_STATES,
            string='Status', readonly=True,
            track_visibility='onchange', copy=False),
        'image': fields.binary(
            'Image', readonly=True,
            states={'draft': [('readonly', False)]},
            help="Incedence Image", copy=False),
        'image_done': fields.binary(
            'Final Image', readonly=True,
            states={'in_production': [('readonly', False)]},
            help="Upload an image to prove this incidence has being fixed",
            copy=False),
        'name': fields.char('Incidence', size=256),
        'description': fields.text(
            'Description', readonly=True,
            states={'draft': [('readonly', False)]},
            help="Thorough Description on incidence being reported",
            copy=False),
        'inspection_report': fields.text(
            'Inspection Report', readonly=True,
            states={'inspection': [('readonly', False)]},
            help="Thorough Description on incidence being reported",
            copy=False),
        'requisitor_id': fields.many2one(
            'res.users', 'Requisitor', readonly=True,
        ),
        'supervisor_id': fields.many2one(
            'res.users', 'Supervisor',
            readonly=True, states={'triage': [('readonly', False)]},
        ),
        'create_date': fields.datetime('Creation Date', readonly=True),
        'task_ids': fields.one2many('project.task', 'incidence_id',
                                    'Activities'),
        'date_start': fields.datetime('Date Start'),
        'date_stop': fields.datetime('Date Stop'),
        'progress': fields.float('Progress'),
        'project_id': fields.many2one('project.project', 'Project',
                                      ondelete='restrict', select=True,
                                      track_visibility='onchange',
                                      change_default=True),
        'picking_type_id': fields.many2one(
            'stock.picking.type', 'Picking Type',
            states={
                'done': [('readonly', True)],
                'cancel': [('readonly', True)]},
            required=False),
        'move_lines':
            fields.one2many('stock.move', 'raw_material_production_id',
                            'Products to Consume',
                            domain=[('state', 'not in', ('done', 'cancel'))],
                            readonly=True,
                            states={'assignment': [('readonly', False)]}),
    }

    _defaults = {
        'state': 'draft',
        'state2': 'draft',
        'name': '/',
        'project_id': _get_default_project,
        'picking_type_id': _get_default_picking_type,
        'product_id': _get_default_product,
        'requisitor_id': lambda s, c, u, cx: s.pool.get('res.users').browse(
            c, u, u, cx).id,
    }

    def create(self, cr, uid, vals, context=None):
        context = context or {}
        if not vals.get('name', False):
            vals['name'] = self.pool.get('ir.sequence').get(
                cr, uid, 'mrp.production') or '/'
        return super(oodoIncidence, self).create(cr, uid, vals, context)

    def actionSend2Triage(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'triage'}, context=context)
        return True

    def actionSupervisorInspection(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'inspection'}, context=context)
        return True

    def actionSubmitted2Supervisor(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'submit'}, context=context)
        for brw in self.browse(cr, uid, ids, context=context):
            self.message_subscribe_users(
                cr, SUPERUSER_ID, [brw.id],
                user_ids=[brw.supervisor_id.id], context=context)
        return True

    def actionResourceAssignment(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'assignment'}, context=context)
        return True

    def actionReturn2Assigment(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'confirmed'}, context=context)
        return True

    def actionPlacePurchase(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'purchase'}, context=context)
        return True

    def actionResourceProcument(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        self.write(cr, uid, ids, {'state': 'procurement'}, context=context)
        return True

    def actionWaitingExecution(self, cr, uid, ids, context=None):
        context = context or {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        if not self.browse(cr, uid, ids[0], context=context).task_ids:
            raise osv.except_osv(
                _('Missing Data!'),
                _('Schedule Tasks First Before continuing!'))
        self.write(cr, uid, ids, {'state': 'waiting'}, context=context)
        return True

    def action_confirm(self, cr, uid, ids, context=None):
        """
        This super creates a Picking for the whole pack of Production moves to
        be consumed in the Production
        """
        res = super(oodoIncidence, self).action_confirm(cr, uid, ids,
                                                        context=context)
        pick_obj = self.pool.get("stock.picking")
        for production in self.browse(cr, uid, ids, context=context):
            move_ids = []
            for line in production.move_lines:
                if not line.picking_id:
                    move_ids.append(line.id)
            if move_ids:
                values = {
                    'origin': production.name,
                    'company_id': production.company_id and
                    production.company_id.id or False,
                    'move_type': 'direct',
                    'picking_type_id': production.picking_type_id and
                    production.picking_type_id.id or False,
                }
                pick_id = pick_obj.create(cr, uid, values, context=context)
            for line in production.move_lines:
                if not line.picking_id:
                    line.write({'picking_id': pick_id})
        return res

    def view_picking(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display existing picking orders of
        given purchase order ids.
        '''
        context = dict(context or {})
        imd_obj = self.pool.get('ir.model.data')
        action_id = tuple(imd_obj.get_object_reference(
            cr, uid, 'stock', 'action_picking_tree'))
        action = self.pool.get('ir.actions.act_window').read(
            cr, uid, action_id[1], context=context)

        pick_ids = []
        for oomm_brw in self.browse(cr, uid, ids, context=context):
            pick_ids += [sm_brw.picking_id.id
                         for sm_brw in oomm_brw.move_lines
                         if sm_brw.picking_id]

        # override the context to get rid of the default filtering on picking
        # type
        action['context'] = {}
        # choose the view_mode accordingly
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(
                map(str, pick_ids)) + "])]"
        else:
            res = imd_obj.get_object_reference(
                cr, uid, 'stock', 'view_picking_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = pick_ids and pick_ids[0] or False
        return action

    def condition_draft_confirm(self, cr, uid, ids, context=None):
        """
        Check if it is possible to go further in the process
        """
        context = dict(context or {})
        ids = isinstance(ids, (int, long)) and [ids] or ids
        if not self.browse(cr, uid, ids[0], context=context).product_lines:
            raise osv.except_osv(
                _('Missing Data!'),
                _('Resources have to be allocated first!'))
        return True

    def test_production_done(self, cr, uid, ids):
        """ Tests whether production is done or not.
        @return: True or False
        """
        res = super(oodoIncidence, self).test_production_done(cr, uid, ids)
        if not res:
            return res
        if not self.browse(cr, uid, ids[0]).image_done:
            raise osv.except_osv(
                _('Missing Data!'),
                _('Please Add a Picture as a Prove this job was done!'))
        return res

class mrp_production_product_line(osv.osv):
    _inherit = 'mrp.production.product.line'

    def _check_product_uom_group(self, cr, uid, context=None):
        group_uom = self.pool.get('ir.model.data').get_object(cr, uid,
                                                              'product',
                                                              'group_uom')
        res = [user for user in group_uom.users if user.id == uid]
        return len(res) and True or False

    def onchange_product_uom(self, cr, uid, ids, product_id, qty, uom_id,
                             name=False, context=None):
        """
        onchange handler of product_uom.
        """
        context = dict(context or {})
        if not uom_id:
            return {'value': {'name': name or '',
                              'product_uom': uom_id or False}}
        context = dict(context, purchase_uom_check=True)
        return self.onchange_product_id(
            cr, uid, ids, product_id, qty, uom_id, name=name, context=context)

    def onchange_product_id(self, cr, uid, ids, product_id, qty, uom_id,
                            name=False, context=None):
        """
        onchange handler of product_id.
        """
        context = dict(context or {})

        res = {'value': {'name': name or '',
                         'product_uom': uom_id or False}}
        if not product_id:
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')

        product = product_product.browse(cr, uid, product_id, context=context)
        res['value'].update({'name': product.name})

        # - set a domain on product_uom
        res['domain'] = {'product_uom': [
            ('category_id', '=', product.uom_id.category_id.id)]}

        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(
                cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') \
                    and self._check_product_uom_group(
                        cr, uid, context=context):
                res['warning'] = {
                    'title': _('Warning!'),
                    'message': _(
                        'Selected Unit of Measure does not belong to the same '
                        'category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        qty = qty or 1.0
        if qty:
            res['value'].update({'product_qty': qty})

        return res
