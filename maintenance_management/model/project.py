# coding: utf-8

from openerp.osv import osv, fields


class project_task(osv.osv):
    _name = "project.task"
    _inherit = ["project.task"]

    _columns = {
        'incidence_id':
            fields.many2one('mrp.production', 'Incidence', ondelete='cascade',
                            select=True),
        'skill_id': fields.many2one(
            'hr.employee.category',
            string='Skill', help='Employee Skills'),
        'employee_id': fields.many2one(
            "hr.employee", string='Employee'),
    }

    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        if not employee_id:
            return {'value': {'user_id': False}}
        emp_obj = self.pool.get('hr.employee').browse(
            cr, uid, employee_id, context=context)
        user_id = False
        if emp_obj.user_id:
            user_id = emp_obj.user_id.id
        return {'value': {'user_id': user_id}}
