# coding: utf-8

from openerp.osv import fields, osv


class res_company(osv.Model):
    _inherit = "res.company"
    _description = 'Companies'

    _columns = {
        'project_id': fields.many2one(
            'project.project', 'Project',
            domain=("[]"),
            required=False),
        'picking_type_id': fields.many2one(
            'stock.picking.type',
            'Picking Type',
            required=False),
        'product_id': fields.many2one(
            'product.product',
            'Product',
            domain=[('type', '=', 'product')]),
    }
