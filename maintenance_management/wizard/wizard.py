# coding: utf-8

from openerp.osv import osv


class mrp_product_produce(osv.osv_memory):
    _inherit = "mrp.product.produce"

    def on_change_mode(self, cr, uid, ids, mode, context=None):
        context = context or {}
        values = {}
        if mode == 'consume':
            values = {'product_qty': 0.0}
        return {'value': values}

    def on_change_qty(self, cr, uid, ids, product_qty, consume_lines, context=None):
        """
            When changing the quantity of products to be produced it will
            recalculate the number of raw materials needed according
            to the scheduled products and the already consumed/produced products
            It will return the consume lines needed for the products to be produced
            which the user can still adapt
        """
        prod_obj = self.pool.get("mrp.production")
        uom_obj = self.pool.get("product.uom")
        production = prod_obj.browse(cr, uid, context['active_id'], context=context)
        consume_lines = []
        new_consume_lines = []

        product_uom_qty = uom_obj._compute_qty(
            cr, uid, production.product_uom.id, product_qty,
            production.product_id.uom_id.id)
        consume_lines = prod_obj._calculate_qty(
            cr, uid, production, product_qty=product_uom_qty, context=context)

        for consume in consume_lines:
            new_consume_lines.append([0, False, consume])
        return {'value': {'consume_lines': new_consume_lines}}
